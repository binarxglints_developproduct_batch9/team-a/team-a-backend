const {
  product,
  cart,
  transaction
} = require('../../models/mongodb')
const {
  user
} = require('../../models/mysql')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator');
const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/';
const storage = multer.diskStorage({
  destination: "./public" + uploadDir,
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})
const upload = multer({
  storage: storage,
  dest: uploadDir
});
module.exports = {
  create: [
    check('productId').custom(async (value, {
      req
    }) => {
      for (var i = 0; i < value.length; i++) {
        var checkProductId = await product.findOne({
          _id: value[i]
        })
        var checkbuyer = await product.findOne({
          _id:value,
          'seller.username':req.user.username
        })
        if (!checkProductId) throw new Error(`product id ${value[i]} doesnt exist!`)
        if (checkbuyer) throw new Error ("sellers can not buy their own product!")
        if (eval(req.body.quantity[i]) > eval(checkProductId.stock.toString())) throw new Error(" Quantity should less than stock of product!")
      }
    }),

    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  updateResi: [
    check('_id').custom(async (value, {
      req
    }) => {

      const checkIdResi = await transaction.findOne({
        _id: value
      })
      if (!checkIdResi) throw new Error("Id Transaction doesn't exist!")

      const checkResi = await transaction.findOne({
        _id: value,
        'productId.seller.username': req.user.username

      })
      if (!checkResi) throw new Error("Only seller has access to input resi!")
    }),

    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },

  ],
  updatePayment: [
    upload.single('payment'),
    check('payment').custom((value, {
      req
    }) => {
      if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage("File must be an image!"),
    check('_id').custom(async (value, {
      req
    }) => {
      const checkIdpayment = await transaction.findOne({
        _id: value
      })
      if (!checkIdpayment) throw new Error("Id Transaction doesn't exist!")
      const checkpayment = await transaction.findOne({
        _id: value,
        'user.username': req.user.username
      })
      const checkStatusCanceled = await transaction.findOne({
        _id: value,
        status: "canceled"
      })
      if (checkStatusCanceled) throw new Error("Your Transaction has been canceled. Please create a new transaction. Make sure you have upload the payment prove before 2 hours.")
      if (!checkpayment) throw new Error("Only buyer has access to upload payment!")
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  delete: [
    check('_id').custom(value => {
      return transaction.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error("Id transaction doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }

  ]

};
