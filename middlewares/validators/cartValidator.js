const {
  product,
  cart
} = require('../../models/mongodb')
const {
  user
} = require('../../models/mysql')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator');


module.exports = {

  getByusername: [
    check('userId').custom(value => {
      return cart.findOne({
        userId: value
      }).then(result => {
        if (!result) {
          throw new Error("ID user doesn't have a cart!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  create: [
    check('productId').custom(async (value, {
      req
    }) => {
      
        var checkProductId = await product.findOne({
          _id: value
        })
        var checkbuyer = await product.findOne({
          _id:value,
          'seller.username':req.user.username
        })
        //TO CHECK AVAILABILITY
        const checkcart = await cart.findOne({
        username: req.user.username,
        "productId": value
      })
        if (checkcart) throw new Error('User already add cart this product!')
        if (!checkProductId) throw new Error("product id doesnt exist!")
        if (checkbuyer) throw new Error ("sellers can not add cart their own product!")
      
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },

  ],
  delete: [
    check('_id').custom(async (value, {
      req
    }) => {
      const checkIdcart = await cart.findOne({
        _id: value
      })
      if (!checkIdcart) throw new Error("Id Cart doesn't exist!")
      const checkCartBuyer = await cart.findOne({
        _id: value,
        username: req.user.username
      })
      if (!checkCartBuyer) throw new Error("Only buyer can delete this cart!")
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
};
