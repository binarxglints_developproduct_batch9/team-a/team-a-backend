const {
  product
} = require('../../models/mongodb')

const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')

const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/';
const storage = multer.diskStorage({
  destination: "./public" + uploadDir,
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {

  getByName: [
    check('keyword').custom(value => {
      return product.find({
        name: {
          $regex: value,
          $options: 'i'
        }
      }).then(result => {
        if (result.length == 0) {
          throw new Error("No product matched!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  getBySeller: [
    check('username').custom(value => {
      return product.find({
        'seller.username': {
          $regex: value,
          $options: 'i'
        }
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Seller doesn't exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  getByCategory: [
    check('category').custom(value => {
      return product.find({
        category: {
          $regex: value,
          $options: 'i'
        }
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Category doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  getOne: [
    check('id').custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  create: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage("File must be an image!"),
    check('name').isString(),
    check('category').custom((value, {
      req
    }) => {
      if (req.body.category.includes("s")) {
        return false
      } else {
        return true
      }
    }).withMessage("Only fruit or vegetable"),
    check('desc').isString(),
    check('price').isNumeric(),
    check('stock').custom((value, {
      req
    }) => {
      if (req.body.stock != '0') {
        return true
      } else {
        return false
      }
    }).withMessage("Stock must be more than 0!"),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage("File must be an image!"),
    check('name').isString(),
    check('category').custom((value, {
      req
    }) => {
      if (req.body.category.includes("s")) {
        return false
      } else {
        return true
      }
    }).withMessage("Only fruit or vegetable"),
    check('desc').isString(),
    check('price').isNumeric(),
    check('stock').custom((value, {
      req
    }) => {
      if (req.body.stock != '0') {
        return true
      } else {
        return false
      }
    }).withMessage("Stock must be more than 0!"),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
