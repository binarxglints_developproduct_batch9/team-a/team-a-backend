const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')
const {
  user
} = require('../../models/mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/img/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {
  signup: [
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_]{3,32}$/)
    .withMessage('username must have 3 to 32 characters without any space'),
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail().withMessage('email must be email address'),
    check('password').notEmpty().withMessage('password cant null').isLength({
      min: 6,
      max: 32
    }).withMessage('password must have 6 to 32 characters'),
    check('passwordConfirmation')
    .notEmpty().withMessage('password confirmation cant null').custom((value, {
      req
    }) => value === req.body.password).withMessage('wrong password confirmation'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  login: [
    check('emailOrUsername').notEmpty().withMessage('email or username cant null')
    .isString().withMessage('input must be username or email address'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  twoFA: [
    check('id').notEmpty().withMessage('id user cant null').custom(value => {
      return user.findOne({
        where: {
          id: value
        }
      }).then(result => {
        if (!result.isTwoFAActive) {
          throw new Error('2FA is not active')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  getOne: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value,
          role: "user"
        }
      }).then(result => {
        if (!result) {
          throw new Error('user not found!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.size > 1 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 1mb'),
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_]{3,32}$/)
    .withMessage('username must have 3 to 32 characters without any space'),
    check('fullName').isString().matches(/^[a-zA-Z\ ']{0,32}$/)
    .withMessage('invalid full name value'),
    check('address').isString().withMessage('must be an address'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  change_password: [
    check('oldPassword').notEmpty().withMessage('old password cant null').custom(async (value, {
      req
    }) => {
      userdata = await user.findOne({
        where: {
          id: req.user.id
        }
      })
      const validate = await bcrypt.compare(value, userdata.password);
      if (!validate) {
        throw new Error('Wrong password!')
      }
    }),
    check('newPassword').notEmpty().withMessage('new password cant null').isLength({
      min: 6,
      max: 32
    })
    .withMessage('password must have 6 to 32 characters'),
    check('newPasswordConfirmation').notEmpty().withMessage('new password confirmation cant null')
    .custom((value, {
      req
    }) => value === req.body.newPassword)
    .withMessage('wrong new password confirmation'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  forgot_password: [
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail()
    .withMessage('Input must be email address')
    .custom(value => {
      return user.findOne({
        where: {
          email: value
        }
      }).then(result => {
        if (!result) {
          throw new Error('User not found')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  reset_password: [
    check('token').custom((value, {
      req
    }) => {
      const decoded = jwt.decode(value)
      return user.findOne({
        where: {
          username: decoded.user.username
        }
      }).then(result => {
        if (!result) {
          throw new Error('User not found')
        }
      })
    }),
    check('newPassword').notEmpty().withMessage('new password cant null').isLength({
      min: 6,
      max: 32
    })
    .withMessage('password must have 6 to 32 characters'),
    check('newPasswordConfirmation').notEmpty().withMessage('new password confirmation cant null')
    .custom((value, {
      req
    }) => value === req.body.newPassword)
    .withMessage('wrong new password confirmation'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  admin_update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.size > 1 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 1mb'),
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_]{3,32}$/)
    .withMessage('username must have 3 to 32 characters without any space'),
    check('fullName').isString().matches(/^[a-zA-Z\ ']{0,32}$/)
    .withMessage('invalid full name value'),
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail()
    .withMessage('Input must be email address')
    .custom(value => {
      return user.findOne({
        where: {
          email: value
        }
      }).then(result => {
        if (result) {
          return false
        }
        return true
      })
    }).withMessage('email already used'),
    check('address').isString().withMessage('must be an address'),
    check('role').toLowerCase().isAlpha()
    .withMessage('role is only user or admin'),
    check('isVerified').isBoolean(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  delete: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value
        },
        paranoid: false
      }).then(result => {
        if (!result) {
          throw new Error('User not found!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  restore: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value
        },
        paranoid: false
      }).then(result => {
        if (!result) {
          throw new Error('User not found!')
        } else if (result.deletedAt = null) {
          throw new Error('User exists, no need to restore')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ]
}
