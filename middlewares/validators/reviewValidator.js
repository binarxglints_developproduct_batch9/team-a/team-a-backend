const {
  review,
  product,
  transaction
} = require('../../models/mongodb')

const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')

module.exports = {

  getByProduct: [
    check('id').custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  create: [
    check('id').custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    check('comment').isString().isLength({
      min: 10,
      max: 200
    }).withMessage("Input between 50-200 characters"),
    check('rating').isNumeric().custom(value => {
      if (value < 0 || value > 5) {
        return false
      } else {
        return true
      }
    }).withMessage("Rating must be around 1 to 5"),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id_product').custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    check('id_review').custom(value => {
      return review.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Review ID doesn't exist!")
        }
      })
    }),
    check('comment').isString().isLength({
      min: 10,
      max: 200
    }).withMessage("Input between 50-200 characters"),
    check('rating').isNumeric().custom(value => {
      if (value < 0 || value > 5) {
        return false
      } else {
        return true
      }
    }).withMessage("Rating must be around 1 to 5"),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return review.findOne({
        _id: value
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Review ID doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
