const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')
const {
  user
} = require('../../models/mysql');
const {
  discuss,
  product
} = require('../../models/mongodb');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/img/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {
  create: [
    check('id_product').isLength({min:24, max:24}).withMessage('invalid object id')
    .isString().custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('product doesn\'t exist')
        }
      })
    }),
    check('question').notEmpty().withMessage('require question')
    .isString().isLength({min:10, max:1000}).withMessage('Must be 10-1000 characters'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  show: [
    check('id_product').isLength({min:24, max:24}).withMessage('invalid object id')
    .isString().custom(value => {
      return product.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('product doesn\'t exist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  comment: [
    check('id').isLength({min:24, max:24}).withMessage('invalid object id')
    .isString().isLength({min:10, max:1000}).withMessage('Must be 10-1000 characters')
    .custom(value => {
      return discuss.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('discussion doesn\'t exist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  delete: [
    check('id').isLength({min:24, max:24}).withMessage('invalid object id')
    .isString().custom(value => {
      return discuss.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('discussion doesn\'t exist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ]
}
