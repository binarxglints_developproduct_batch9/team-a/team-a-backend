const path = require('path')
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})
const passport = require('passport');
const https = require('https');
const localStrategy = require('passport-local').Strategy
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const {
  user
} = require('../../models/mysql');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt

passport.use(
  'signup',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      try {
        // Create new user with email, password and role
        let createdUser = await user.create({
          username: req.body.username,
          email: email,
          password: password,
          role: 'user',
          image: 'db23a4ce3ce287ec2b2591963f956cff.png',
          isVerified: 0,
          isTwoFAActive: 0
        });

        // Find new user that have been created in advance
        let newUser = await user.findOne({
          where: {
            id: createdUser.id
          },
          attributes: ['id', 'username']
        });

        // If success, it will return newUser variable that can be used in the next step
        return done(null, newUser, {
          message: 'Signup success!'
        });
      } catch (err) {
        if (err) {
          if (err.code = 'ER_DUP_ENTRY') {
            return done(null, false, {
              message: 'Username or Email already used!'
            })
          }
          return done(null, false, {
            message: 'User failed to created!'
          })
        }
      }
    },
  )
)

passport.use(
  'google',
  new GoogleStrategy({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        let userLogin = await user.findOne({
          where: {
            email: profile.emails[0].value
          }
        })
        if (!userLogin) {
          userLogin = await user.create({
            username: profile.name.givenName,
            fullName: profile.displayName,
            email: profile.emails[0].value,
            password: "sayonara" + profile.name.givenName,
            role: 'user',
            image: 'db23a4ce3ce287ec2b2591963f956cff.png',
            isVerified: profile.emails[0].verified,
            isTwoFAActive: 0
          });
        }
        return done(null, userLogin)
      } catch (error) {
        return done(null, false)
      }
    }
  )
);

passport.use(
  'login',
  new localStrategy({
      'usernameField': 'emailOrUsername',
      'passwordField': 'password'
    },
    async (emailOrUsername, password, done) => {
      const userLogin = await user.findOne(
        emailOrUsername.includes('@') ? {
          where: {
            email: emailOrUsername
          }
        } : {
          where: {
            username: emailOrUsername
          }
        }
      )
      if (!userLogin) {
        return done(null, false, {
          message: 'User not found!'
        })
      }
      const validate = await bcrypt.compare(password, userLogin.password);
      if (!validate) {
        return done(null, false, {
          message: 'Wrong password!'
        })
      }
      if (userLogin.isVerified == false) {
        return done(null, false, {
          message: 'Your account has not been verified, check your email to verify'
        })
      }
      if (userLogin.isVerified == true) {
        return done(null, userLogin)
      }
    }
  )
)

passport.use(
  'profile',
  new JWTstrategy({
      secretOrKey: 'hashPassword', // It must be same with secret key when created token
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(), // It will extract token from req.header('Authorization')
      passReqToCallback: true // Enable req for next step function
    },
    async (req, token, done) => {
      try {
        const userLogin = await user.findOne({
         where:{ id: token.user.id }
        })
        if (!userLogin) {
          return done(null, false, {
            message: 'User not found!'
          })
        }
        if (userLogin.isVerified == false) {
          return done(null, false, {
            message: 'Email not verified yet, check your email to verify'
          })
        }
        if (userLogin.isVerified == true) {
          return done(null, token.user)
        }
      } catch (e) {
        // If error, it will create this message
        return done(null, false, {
          message: "Unauthorized!"
        });
      }
    }
  )
);

passport.use(
  'admin',
  new JWTstrategy({
      secretOrKey: 'hashPassword',
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      passReqToCallback: true
    },
    async (req, token, done) => {
      const userLogin = await user.findOne({
        where: {
          id: token.user.id
        },
        attributes: ['id', 'username', 'role']
      })
      if (userLogin.role == "admin") {
        return done(null, userLogin)
      }
      return done(null, false)
    }
  )
)
