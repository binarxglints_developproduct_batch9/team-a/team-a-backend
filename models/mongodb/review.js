const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete')

const ReviewSchema = new mongoose.Schema({
  buyer: {
    username: {
      type: mongoose.Schema.Types.Mixed,
      required: true
    },
    image: {
      type: String,
      required: true
    }
  },
  comment: {
    type: String,
    required: true
  },
  product: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  rating: {
    type: mongoose.Schema.Types.Decimal128,
    required: true
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false,
  toJSON: {
    getters: true
  }
});

ReviewSchema.path('buyer.image').get((v) => {
  return "/img/" + v
})

ReviewSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = review = mongoose.model('review', ReviewSchema, 'review');
