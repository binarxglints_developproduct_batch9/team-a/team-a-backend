const path = require('path');
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})
const mongoose = require('mongoose');

const uri = process.env.MONGO_URI


mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
})

const product = require('./product')
const cart = require('./cart')
const discuss = require('./discuss')
const review = require('./review')
const transaction = require('./transaction.js')



module.exports = {
  product,
  cart,
  discuss,
  transaction,
  review
}
