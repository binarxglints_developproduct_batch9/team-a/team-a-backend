const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');
const Schema = mongoose.Schema
const CartSchema = new Schema({
  username: {
    type: Schema.Types.Mixed,
    required: true
  },
  productId: {
    type: Schema.Types.ObjectId,
    ref: 'product',
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    uspdatedAt: 'updated_at'
  },
  versionKey: false
});
CartSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});
module.exports = cart = mongoose.model('cart', CartSchema, 'cart');