const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');
const Schema = mongoose.Schema
const TransactionSchema = new Schema({
  user: {
    type: Schema.Types.Mixed,
    required: true
  },
  productId: {
    type: Schema.Types.Mixed,
    required:true
  },
  status: {
    type: String,
    required: false,
    default: "pending"
  },
  resi: {
    type: String,
    required: false,
    default: null
  },
  payment: {
    type: String,
    default: null,
    required: false,
    get: getImage
  },
  total: {
    type: mongoose.Schema.Types.Decimal128,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false,
  toJSON: { getters: true }
});

function getImage(payment) {
  return '/img/' + payment;
}

 TransactionSchema.path('productId').get((v) => {
  for(let i = 0; i<v.length; i++){
    v[i].seller.image = "/img/" + v[i].seller.image
    v[i].image = "/img/" + v[i].image
  }
  return v
}) 


TransactionSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = transaction = mongoose.model('transaction', TransactionSchema, 'transaction');
