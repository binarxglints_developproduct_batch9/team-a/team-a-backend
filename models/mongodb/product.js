const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete')

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: null,
    required: false,
    get: getImage
  },
  price: {
    type: mongoose.Schema.Types.Decimal128,
    required: true
  },
  stock: {
    type: mongoose.Schema.Types.Decimal128,
    required: true
  },
  quantity: {
    type: Number,
    required: false
  },
  category: {
    type: String,
    required: true
  },
  desc: {
    type: String,
    required: true
  },
  seller: {
    username: {
      type: mongoose.Schema.Types.Mixed,
      required: true
    },
    image: {
      type: String,
      required: true
    }
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false,
  toJSON: {
    getters: true
  }
});

function getImage(image) {
  return '/img/' + image;
}

ProductSchema.path('seller.image').get((v) => {
  return '/img/' + v
})

ProductSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = product = mongoose.model('product', ProductSchema, 'product');
