const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');

//id review, id user, id movie, headline, isi review, rating
const DiscussSchema = new mongoose.Schema({
  product: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref:'product'
  },
  commentator: {
    id: {type: Number, require:true},
    username: {type: String, require:true},
    fullName: {type: String, require:true},
    image: {type: String, require:true}
  },
  question: {
    type: String,
    required: true
  },
  answer: {
    type: mongoose.Schema.Types.Mixed,
    required: false,
    get: getAnswer
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false,
  toJSON: { getters: true }
});

function getAnswer(value) {
  if (value.length != 0) {
    value.map(data => {
      data.image = "/img/" + data.image;
    });
  }

  return value;
}

DiscussSchema.path('commentator.image').get((v)=>{
  return '/img/'+v
})

DiscussSchema.set('toJSON', {getters:true})

DiscussSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = discuss = mongoose.model('discuss', DiscussSchema, 'discuss');
