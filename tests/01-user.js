//Require the dev-dependencies
let chai = require('chai'); // import chai for testing assert
let chaiHttp = require('chai-http'); // make virtual server to get/post/put/delete
let server = require('../index'); // import app from index
let should = chai.should(); // import assert should from chai
let jwt = require('jsonwebtoken')

chai.use(chaiHttp); // use chaiHttp

let token

describe('User', () => {
  describe('/POST Sign Up', () => {
    it('It should make user and link confirmation to email', (done) => {
      chai.request(server)
      .post('/signup')
      .send({
        email: 'usertest@gmail.com',
        password: '123456',
        passwordConfirmation: '123456',
        username: 'users_test'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('message').eql('Register success, check your email to verify!')
        done()
      })
    })
  })

  describe('verify user email', () => {
    it('It should verify user account', (done) => {
      const body = {
        username: 'users_test',
        id: '1'
      }

      const token = jwt.sign({
        user: body
      }, 'hashPassword', {
        expiresIn: '1h'
      })
      chai.request(server)
      .get(`/verification/${token}`)
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        done()
      })
    })
  })

  describe('LOGIN user', () => {
    it('it should get user token', (done) => {
      chai.request(server)
      .post('/login')
      .send({
        emailOrUsername: 'users_test',
        password: '123456'
      })
      .end((err, res) => {
        token = res.body.token
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('message').eql('Login success!')
        res.body.should.have.property('token')
        done()
      })
    })
  })

  // describe('show our profile', () => {
  //   it('get own profile', (done) => {
  //     chai.request(server)
  //     .get('/profile')
  //     .set({Authorization : `Bearer ${token}`})
  //     .end((err, res) => {
  //       res.should.have.status(200)
  //       res.body.should.be.an('object')
  //       res.body.should.have.property('status')
  //       res.body.should.have.property('data')
  //       res.body.data.should.be.an('object')
  //       res.body.data.should.have.property('image')
  //       res.body.data.should.have.property('id')
  //       res.body.data.should.have.property('email')
  //       res.body.data.should.have.property('username')
  //       res.body.data.should.have.property('fullName')
  //       res.body.data.should.have.property('password')
  //       res.body.data.should.have.property('rating')
  //       res.body.data.should.have.property('role')
  //       res.body.data.should.have.property('isVerified')
  //       res.body.data.should.have.property('isTwoFAActive')
  //       res.body.data.should.have.property('secret')
  //       res.body.data.should.have.property('createdAt')
  //       res.body.data.should.have.property('updatedAt')
  //       res.body.data.should.have.property('deletedAt')
  //       done()
  //     })
  //   })
  // })

  // describe('update profile', () => {
  //   it('it should update user profile', (done) => {
  //     chai.request(server)
  //     .put('/profile/update')
  //     .set({Authorization : `Bearer ${token}`})
  //     .attach('image', './public/img/db23a4ce3ce287ec2b2591963f956cff.png')
  //     .field({
  //       username: 'users_test',
  //       fullName: 'user tests'
  //     })
  //     .end((err, res) => {
  //       res.should.have.status(200)
  //       res.body.should.be.an('object')
  //       res.body.should.have.property('status')
  //       res.body.should.have.property('data')
  //       res.body.data.should.be.an('object')
  //       res.body.data.should.have.property('image')
  //       res.body.data.should.have.property('id')
  //       res.body.data.should.have.property('email')
  //       res.body.data.should.have.property('username')
  //       res.body.data.should.have.property('fullName')
  //       res.body.data.should.have.property('rating')
  //       done()
  //     })
  //   })
  // })

  // describe('show other user profile', () => {
  //   it('it should show other user profile', (done) => {
  //     chai.request(server)
  //     .get('/profile/users_test')
  //     .end((err, res) => {
  //       res.should.have.status(200)
  //       res.body.should.be.an('object')
  //       res.body.should.have.property('status')
  //       res.body.should.have.property('data')
  //       res.body.data.should.be.an('object')
  //       res.body.data.should.have.property('image')
  //       res.body.data.should.have.property('id')
  //       res.body.data.should.have.property('username')
  //       res.body.data.should.have.property('fullName')
  //       res.body.data.should.have.property('rating')
  //       done()
  //     })
  //   })
  // })

  describe('change password', () => {
    it('it should change password', (done) => {
      chai.request(server)
      .put('/change_password')
      .set({Authorization : `Bearer ${token}`})
      .send({
        oldPassword: '123456',
        newPassword: '123456',
        newPasswordConfirmation: '123456'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('status').eql("Success change password")
        done()
      })
    })
  })

  describe('forgot password', () => {
    it('it should send email a link to reset password', (done) => {
      chai.request(server)
      .post(`/forgot_password`)
      .send({
        email: 'usertest@gmail.com'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('message').eql("Link to reset password has been sent to your email")
        done()
      })
    })
  })

  describe('reset password', () => {
    it('it should reset password and set new password', (done) => {
      chai.request(server)
      .put(`/reset_password/${token}`)
      .send({
        newPassword: '123456',
        newPasswordConfirmation: '123456'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('status').eql("Success change password")
        res.body.should.have.property('message').eql("Login with new password to continnue")
        done()
      })
    })
  })

  describe('Switch 2FA on or off', () => {
    it('it should turn on 2FA', (done) => {
      chai.request(server)
      .put('/switch')
      .set({Authorization : `Bearer ${token}`})
      .send({
        isTwoFAActive: 'true'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('status').eql('2FA status on')
        res.body.should.have.property('result')
        res.body.result.should.be.an('object')
        res.body.result.should.have.property('secret')
        res.body.result.should.have.property('uri')
        res.body.result.should.have.property('qr')
        done()
      })
    })
  })

  describe('LOGIN user if 2FA On', () => {
    it('it should get user id', (done) => {
      chai.request(server)
      .post('/login')
      .send({
        emailOrUsername: 'users_test',
        password: '123456'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('message').eql('Login success!')
        res.body.should.have.property('userId')
        done()
      })
    })
  })

  describe('Switch 2FA on or off', () => {
    it('it should turn off 2FA', (done) => {
      chai.request(server)
      .put('/switch')
      .set({Authorization : `Bearer ${token}`})
      .send({
        isTwoFAActive: 'false'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('status').eql('2FA status off')
        done()
      })
    })
  })
})
