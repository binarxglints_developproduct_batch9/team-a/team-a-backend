// //Require the dev-dependencies
// let chai = require('chai'); // import chai for testing assert
// let chaiHttp = require('chai-http'); // make virtual server to get/post/put/delete
// let server = require('../index'); // import app from index
// let should = chai.should(); // import assert should from chai
// let {user} = require('../models/mysql')
// let {discuss, product} = require('../models/mongodb')
// let jwt = require('jsonwebtoken')
//
// chai.use(chaiHttp); // use chaiHttp
//
// let token
// let id_product
// let id_discussion
//
// describe('Discuss', () => {
//   describe('/POST Discuss Question', () => {
//     it('It should make question for discussion', (done) => {
//       const body = {
//         username: 'users_test',
//         id: '1'
//       }
//
//       const token = jwt.sign({
//         user: body
//       }, 'hashPassword', {
//         expiresIn: '1h'
//       })
//       chai.request(server)
//       .post('/home/create/')
//       .set({Authorization : `Bearer ${token}`})
//       .attach('image', 'tests/wortel.jpg')
//       .field({
//         name: "Wortel",
//         price: 5000,
//         desc: "Wortel is good for eyes",
//         stock: 500,
//         category: "vegetable"
//       })
//       .end((err, res) => {
//         const body = {
//           username: 'users_test',
//           id: '1'
//         }
//
//         const token = jwt.sign({
//           user: body
//         }, 'hashPassword', {
//           expiresIn: '1h'
//         })
//         id_product = res.body.data.id
//         chai.request(server)
//         .post(`/discuss/${id_product}/create`)
//         .set({Authorization : `Bearer ${token}`})
//         .send({
//           question: 'apakah masih ada?'
//         })
//         .end((err, res) => {
//           id_discussion = res.body.data.id
//           res.should.have.status(200)
//           res.body.should.be.an('object')
//           res.body.should.have.property('status').eql('Success!')
//           res.body.should.have.property('data')
//           res.body.data.should.be.an('object')
//           res.body.data.should.have.property('_id')
//           res.body.data.should.have.property('deleted')
//           res.body.data.should.have.property('product')
//           res.body.data.should.have.property('commentator')
//           res.body.data.commentator.should.be.an('object')
//           res.body.data.commentator.should.have.property('id')
//           res.body.data.commentator.should.have.property('username')
//           res.body.data.commentator.should.have.property('fullName')
//           res.body.data.commentator.should.have.property('image')
//           res.body.data.should.have.property('question')
//           res.body.data.should.have.property('answer')
//           res.body.data.answer.should.be.an('array')
//           res.body.data.should.have.property('created_at')
//           res.body.data.should.have.property('updated_at')
//           res.body.data.should.have.property('id')
//           done()
//         })
//       })
//     })
//   })
//
//   describe('/PUT comment to discussion', () => {
//     it('It should PUT comment to the discussion', (done) => {
//       const body = {
//         username: 'users_test',
//         id: '1'
//       }
//
//       const token = jwt.sign({
//         user: body
//       }, 'hashPassword', {
//         expiresIn: '1h'
//       })
//       chai.request(server)
//       .put(`/discuss/${id_discussion}/comment`)
//       .set({Authorization : `Bearer ${token}`})
//       .send({
//         comment: 'masih gan'
//       })
//       .end((err, res) => {
//         res.should.have.status(200)
//         res.body.should.be.an('object')
//         res.body.should.have.property('status').eql('Success!')
//         res.body.should.have.property('data')
//         res.body.data.should.be.an('object')
//         res.body.data.should.have.property('_id')
//         res.body.data.should.have.property('deleted')
//         res.body.data.should.have.property('product')
//         res.body.data.should.have.property('commentator')
//         res.body.data.commentator.should.be.an('object')
//         res.body.data.commentator.should.have.property('id')
//         res.body.data.commentator.should.have.property('username')
//         res.body.data.commentator.should.have.property('fullName')
//         res.body.data.commentator.should.have.property('image')
//         res.body.data.should.have.property('question')
//         res.body.data.should.have.property('answer')
//         res.body.data.answer.should.be.an('array')
//         res.body.data.should.have.property('created_at')
//         res.body.data.should.have.property('updated_at')
//         res.body.data.should.have.property('id')
//         done()
//       })
//     })
//   })
//
//   describe('/GET product discussion', () => {
//     it('It should shown a product discussion', (done) => {
//       const body = {
//         username: 'users_test',
//         id: '1'
//       }
//
//       const token = jwt.sign({
//         user: body
//       }, 'hashPassword', {
//         expiresIn: '1h'
//       })
//       chai.request(server)
//       .get(`/discuss/${id_product}`)
//       .set({Authorization : `Bearer ${token}`})
//       .end((err, res) => {
//         res.should.have.status(200)
//         res.body.should.be.an('object')
//         res.body.should.have.property('status')
//         res.body.should.have.property('data')
//         res.body.data.should.be.an('array')
//         done()
//       })
//     })
//   })
//
//   // describe('/DELETE discussion', () => {
//   //   it('It should DELETE discussion', (done) => {
//   //     chai.request(server)
//   //     .post(`/discuss/60176aea7cc2965746316bd1/create`)
//   //     .set({Authorization : `Bearer ${token}`})
//   //     .send({
//   //       question: 'apakah masih ada?'
//   //     })
//   //     .end((err, res) => {
//   //       chai.request(server)
//   //       .put(`/discuss/delete/${res.body.data._id}`)
//   //       .set({Authorization : `Bearer ${token}`})
//   //       .end((err,res) => {
//   //         res.should.have.status(200)
//   //         res.body.should.be.an('object')
//   //         res.body.should.have.property('status').eql('Success delete data!')
//   //         done()
//   //       })
//   //     })
//   //   })
//   // })
// })
