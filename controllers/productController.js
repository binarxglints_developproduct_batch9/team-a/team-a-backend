const {
  product,
  cart
} = require('../models/mongodb')
const {
  user
} = require('../models/mysql')

class ProductController {

  async getAll(req, res) {
    product.find({}, 'name price image').then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async carousel(req, res) {
    product.find({}, 'image').limit(3).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getBySeller(req, res) {
    product.find({
      'seller.username': {
        $regex: req.params.username,
        $options: 'i'
      }
    }, 'name image price stock desc category').then(result => {
      res.json({
        staus: "Success",
        data: result
      })
    })
  }


  async getByName(req, res) {
    product.find({
      name: {
        $regex: req.params.keyword,
        $options: 'i'
      }
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getOne(req, res) {
    product.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getByCategory(req, res) {
    product.find({
      category: req.params.category
    }, 'name price image').then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {

    const seller = await user.findOne({
      where: {
        username: req.user.username
      },
      attributes: ['username', 'image']
    })

    product.create({
      name: req.body.name,
      price: req.body.price,
      desc: req.body.desc,
      stock: req.body.stock,
      category: req.body.category,
      seller: seller.dataValues,
      image: req.file === undefined ? "" : req.file.filename,
    }).then(result => {
      res.json({
        status: "Succcess add product",
        data: result
      })
    })
  }

  async update(req, res) {

    const seller = await user.findOne({
        where:{ username: req.user.username}
    })

    await product.findOneAndUpdate({
      _id: req.params.id
    }, {
      name: req.body.name,
      price: req.body.price,
      desc: req.body.desc,
      stock: req.body.stock,
      category: req.body.category,
      seller: seller.dataValues,
      image: req.file === undefined ? "" : req.file.filename,
    }, {
      new: true
    }).then(result => {
      res.json({
        status: "Success update product!",
        data: result
      })
    })
  }

  async delete(req, res) {
    product.delete({
        _id: req.params.id
      })
      .then(async (result) => {
        await cart.delete({
          productId: req.params.id

        })
        res.json({
          status: "Success delete product!",
          data: null
        })
      })
  }
}

module.exports = new ProductController
