const {
  user
} = require('../models/mysql');
const {
  discuss,
  review,
  product,
  cart,
  transaction
} = require('../models/mongodb');
const passport = require('passport');
const twofactor = require("node-2fa");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  secure: true,
  auth: {
    user: 'ecomsayonara@gmail.com',
    pass: 'sayonaraglints2021'
  },
  tls: {
    rejectUnauthorized: false
  }
}))

class UserController {
  async signup(user, req, res) {
    const body = {
      username: user.username,
      id: user.id
    }
    const token = jwt.sign({
      user: body
    }, 'hashPassword')
    const mailOptions = {
      from: 'ecomsayonara@gmail.com',
      to: req.body.email,
      subject: 'Account Verification',
      text: `Hello, ${user.username}\n\n` + 'If you\'re register to Sayonara click link below to verify your account\n link : https://sayonara.vercel.app/verification/' + token + '\n\n You can ignore this email if is not you'
    }
    // req.headers.host
    await transporter.sendMail(mailOptions)
    res.status(200).json({
      message: "Register success, check your email to verify!"
    })
  }

  async failed(req, res) {
    try {
      console.log(res);
      res.status(500).json({
        message: "Internal Server Error"
      })
    } catch (e) {
      res.status(500), json({
        message: "Internal Server Error",
        error: e
      })
    }
  }

  async login(utoken, req, res) {
    const is2FAActive = await user.findOne({
      where: {
        id: utoken.id
      }
    })
    if (!is2FAActive.isTwoFAActive) {
      const body = {
        username: utoken.username,
        id: utoken.id
      }
      const token = jwt.sign({
        user: body
      }, 'hashPassword')
      res.status(200).json({
        message: "Login success!",
        token: token
      })
    } else if (is2FAActive.isTwoFAActive) {
      res.status(200).json({
        message: "Login success!",
        userId: utoken.id
      })
    }
  }

  async loginGoogle(req, res) {
    const is2FAActive = await user.findOne({
      where: {
        id: req.user.id
      }
    })
    if (!is2FAActive.isTwoFAActive) {
      const body = {
        username: req.user.username,
        id: req.user.id
      }
      const token = jwt.sign({
        user: body
      }, 'hashPassword', {
        expiresIn: '1h'
      })
      res.status(200).json({
        message: "Login success!",
        token: token
      })
    } else if (is2FAActive.isTwoFAActive) {
      res.status(200).json({
        message: "Login success!",
        userId: req.user.id
      })
    }
  }

  async switch (req, res) {
    try {
      const newSecret = twofactor.generateSecret({
        id: req.user.id
      });
      if (req.body.isTwoFAActive == 'true') {
        let stat = 'on'
        await user.update({
          isTwoFAActive: req.body.isTwoFAActive,
          secret: newSecret.secret
        }, {
          where: {
            id: req.user.id
          }
        })
        return res.status(200).json({
          status: `2FA status ${stat}`,
          result: newSecret
        })
      } else if (req.body.isTwoFAActive == 'false') {
        let stat = 'off'
        await user.update({
          isTwoFAActive: req.body.isTwoFAActive,
          secret: newSecret.secret
        }, {
          where: {
            id: req.user.id
          }
        })
        return res.status(200).json({
          status: `2FA status ${stat}`
        })
      }
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async twoFA(req, res) {
    try {
      const is2FAActive = await user.findOne({
        where: {
          id: req.body.id
        }
      })
      const body = {
        username: is2FAActive.username,
        id: is2FAActive.id
      }
      const token = jwt.sign({
        user: body
      }, 'hashPassword', {
        expiresIn: '1h'
      })
      let status
      const verify = twofactor.verifyToken(is2FAActive.secret, req.body.code)
      if (verify == null) {
        return res.status(200).json({
          status: "Incorrect Code"
        })
      } else if (verify.delta == 0) {
        return res.status(200).json({
          status: "Correct",
          token: token
        })
      } else {
        return res.status(200).json({
          status: "Expired Code"
        })
      }
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async verification(req, res) {
    try {
      const decoded = jwt.decode(req.params.token)
      const validate = await user.findOne({
        where: {
          username: decoded.user.username
        },
        attributes: ['username', 'id']
      })
      if (!validate) {
        throw new Error('User not found')
      }
      user.update({
        isVerified: 1
      }, {
        where: {
          username: decoded.user.username
        }
      })
      // If success, it will be return the user information (id, email, and role)
      return res.status(200).json({
        status: "Verification Success",
        message: "Login to continnue"
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: "Unauthorized!"
      })
    }
  }

  async getOne(req, res) {
    const allrate = await review.find({
      "product.seller.username": req.user.username
    })

    var sum = 0;
    for (var i = 0; i < allrate.length; i++) {
      sum += eval(allrate[i].rating.toString())
      var avg = sum / allrate.length
    }
    await user.update({
      rating: avg
    }, {
      where: {
        id: req.user.id
      }
    })
    const result = await user.findOne({
      where: {
        id: req.user.id
      }
    })
    res.json({
      status: "Succes get data",
      data: result
    })
  }

  async getOneO(req, res) {
    const allrate = await review.find({
      "product.seller.username": req.params.username
    })

    var sum = 0;
    for (var i = 0; i < allrate.length; i++) {
      sum += eval(allrate[i].rating.toString())
      var avg = sum / allrate.length
    }
    await user.update({
      rating: avg
    }, {
      where: {
        username: req.params.username
      }
    })
    user.findOne({
      where: {
        username: req.params.username,
        role: "user"
      },
      attributes: ['id', 'username', 'fullName', 'image', 'address', 'rating']
    }).then(result => {
      res.json({
        status: "Succes get data",
        data: result
      })
    })
  }

  async update(req, res) {
    const prevUser = await user.findOne({
      where: {
        id: req.user.id
      }
    })
    await user.update({
      fullName: req.body.fullName,
      username: req.body.username,
      address: req.body.address,
      image: req.file === undefined ? prevUser.image.substr(5) : req.file.filename
    }, {
      where: {
        id: req.user.id
      }
    })
    let newTSProfile = await discuss.update({
      "commentator.id": req.user.id
    }, {
      $set: {
        "commentator.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "commentator.username": req.body.username,
        "commentator.fullName": req.body.fullName
      }
    }, {
      multi: true
    })
    let newCartProfile = await cart.update({
      "username": prevUser.username
    }, {
      $set: {
        "username": req.body.username
      }
    }, {
      multi: true
    })
    let newCommentatorProfile = await discuss.update({
      "answer.id": req.user.id
    }, {
      $set: {
        "answer.$[].image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "answer.$[].username": req.body.username,
        "answer.$[].fullName": req.body.fullName
      }
    }, {
      multi: true
    })
    let newProductSellerProfile = await product.update({
      "seller.username": prevUser.username
    }, {
      $set: {
        "seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newReviewerProfile = await review.update({
      "buyer.username": prevUser.username
    }, {
      $set: {
        "buyer.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "buyer.username": req.body.username
      }
    }, {
      multi: true
    })
    let newPOReviewProfile = await review.update({
      "product.seller.username": prevUser.username
    }, {
      $set: {
        "product.seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "product.seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newTransactionSellerProfile = await transaction.update({
      "productId.seller.username": prevUser.username
    }, {
      $set: {
        "productId.$[].seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "productId.$[].seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newTransactionBuyerProfile = await transaction.update({
      "user.id": req.user.id
    }, {
      $set: {
        // "user.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "user.username": req.body.username
      }
    }, {
      multi: true
    })
    const newUser = await user.findOne({
      where: {
        id: req.user.id
      },
      attributes: ['id', 'username', 'fullName', 'image', 'address', 'email', 'rating']
    })
    return res.json({
      status: "Succes updating data",
      data: newUser
    })
  }

  async getAll(req, res) {
    user.findAll({
      where: {
        role: 'user'
      },
      paranoid: false
    }).then(result => {
      res.json({
        status: "Success get all data",
        data: result
      })
    })
  }

  async change_password(req, res) {
    user.update({
      password: req.body.newPassword
    }, {
      where: {
        id: req.user.id
      }
    }).then(() => {
      res.json({
        status: "Success change password"
      })
    })
  }

  async forgot_password(req, res) {
    const body = await user.findOne({
      where: {
        email: req.body.email
      },
      attributes: ['id', 'username']
    })
    const token = jwt.sign({
      user: body
    }, 'hashPassword')
    const mailOptions = {
      from: 'ecomsayonara@gmail.com',
      to: req.body.email,
      subject: 'Reset Password',
      text: `Hello, ${body.username}\n\n` + 'Click link below to reset password\n link : https://sayonara.vercel.app/reset_password/' + token + '\n\n You can ignore this email if you\'re not request to reset password'
    }
    await transporter.sendMail(mailOptions)
    res.status(200).json({
      message: "Link to reset password has been sent to your email"
    })
  }

  async reset_password(req, res) {
    try {
      const decoded = jwt.decode(req.params.token)
      user.update({
        password: req.body.newPassword
      }, {
        where: {
          username: decoded.user.username
        }
      })
      // If success, it will be return the user information (id, email, and role)
      return res.status(200).json({
        status: "Success change password",
        message: "Login with new password to continnue"
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: "Request Failed"
      })
    }
  }

  async admin_update(token, req, res) {
    const prevUser = await user.findOne({
      where: {
        username: req.params.username
      }
    })
    await user.update({
      fullName: req.body.fullName,
      username: req.body.username,
      address: req.body.address,
      image: req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
      role: req.body.role,
      email: req.body.email,
      isVerified: req.body.isVerified
    }, {
      where: {
        username: req.params.username
      }
    })
    let newTSProfile = await discuss.update({
      "commentator.id": prevUser.id
    }, {
      $set: {
        "commentator.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "commentator.username": req.body.username,
        "commentator.fullName": req.body.fullName
      }
    }, {
      multi: true
    })
    let newCommentatorProfile = await discuss.update({
      "answer.id": prevUser.id
    }, {
      $set: {
        "answer.$[].image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "answer.$[].username": req.body.username,
        "answer.$[].fullName": req.body.fullName
      }
    }, {
      multi: true
    })
    let newCartProfile = await cart.update({
      "username": prevUser.username
    }, {
      $set: {
        "username": req.body.username
      }
    }, {
      multi: true
    })
    let newProductSellerProfile = await product.update({
      "seller.username": prevUser.username
    }, {
      $set: {
        "seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newReviewerProfile = await review.update({
      "buyer.username": prevUser.username
    }, {
      $set: {
        "buyer.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "buyer.username": req.body.username
      }
    }, {
      multi: true
    })
    let newPOReviewProfile = await review.update({
      "product.seller.username": prevUser.username
    }, {
      $set: {
        "product.seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "product.seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newTransactionSellerProfile = await transaction.update({
      "productId.seller.username": prevUser.username
    }, {
      $set: {
        "productId.$[].seller.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "productId.$[].seller.username": req.body.username
      }
    }, {
      multi: true
    })
    let newTransactionBuyerProfile = await transaction.update({
      "user.id": prevUser.id
    }, {
      $set: {
        // "user.image": req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        "user.username": req.body.username
      }
    }, {
      multi: true
    })
    const result = await user.findOne({
      where: {
        username: req.body.username
      }
    })
    res.json({
      status: "Success updating data",
      data: result
    })
  }

  async delete(token, req, res) {
    try {
      user.destroy({
        where: {
          username: req.params.username
        }
      })
      return res.status(200).json({
        status: 'Success!',
        message: 'User deleted'
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error!',
        message: 'Request failed!'
      })
    }
  }

  async restore(token, req, res) {
    try {
      await user.restore({
        where: {
          username: req.params.username
        }
      })
      const result = await user.findOne({
        where: {
          username: req.params.username
        }
      })
      return res.status(200).json({
        status: 'Success!',
        data: result
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error',
        message: 'Restore data user failed'
      })
    }
  }

  async hard_deleted(token, req, res) {
    try {
      user.destroy({
        where: {
          username: req.params.username
        },
        force: true
      })
      return res.status(200).json({
        status: 'Success!',
        message: 'User deleted'
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error!',
        message: 'Request failed!'
      })
    }
  }
}

module.exports = new UserController
