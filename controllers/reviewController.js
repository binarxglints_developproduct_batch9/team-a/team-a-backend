const {
  review,
  product
} = require('../models/mongodb')
const {
  user
} = require('../models/mysql')

class ReviewController {

  async getByProduct(req, res) {
    const theProduct = await product.find({
      _id: req.params.id
    })

    review.find({
      product: theProduct
    }, 'buyer comment rating').then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {

    const theProduct = await product.findOne({
      _id: req.params.id
    })

    const theUser = await user.findOne({
      where: {
        username: req.user.username
      },
      attributes: ['username', 'image']
    })

    review.create({
      product: theProduct,
      buyer: theUser.dataValues,
      comment: req.body.comment,
      rating: req.body.rating
    }).then(result => {
      res.json({
        status: "Success add review",
        data: result
      })
    })
  }

  async update(req, res) {

    const theProduct = await product.findOne({
      _id: req.params.id_product
    })

    const theUser = await user.findOne({
      where: {
        username: req.user.username
      },
      attributes: ['username', 'image']
    })

    review.findOneAndUpdate({
      _id: req.params.id_review
    }, {
      product: theProduct,
      buyer: theUser.dataValues,
      comment: req.body.comment,
      rating: req.body.rating
    }, {
      new: true
    }).then(result => {
      res.json({
        status: "Success update review",
        data: result
      })
    })
  }

  async delete(req, res) {
    review.delete({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success delete review!",
        data: null
      })
    })
  }

}

module.exports = new ReviewController
