const {
  product,
  discuss
} = require('../models/mongodb')
const {
  user
} = require('../models/mysql')

class DiscussController {
  async create(req, res) {
    try {
      const commentator = await user.findOne({
        where: {
          username: req.user.username
        },
        attributes: ['id', 'username', 'fullName', 'image']
      })
      const theProduct = await product.find({
        _id: req.params.id_product
      }, 'id name image price seller')
      discuss.create({
        product: theProduct[0]._id,
        commentator: {
          id: commentator.dataValues.id,
          username: commentator.dataValues.username,
          fullName: commentator.dataValues.fullName,
          image: commentator.dataValues.image
        },
        question: req.body.question,
        answer: []
      }).then(result => {
        return res.status(200).json({
          status: 'Success!',
          data: result
        })
      })
    } catch (e) {
      console.log(e);
      return res.status(401).json({
        status: 'Error',
        message: 'Request failed'
      })
    }
  }

  async show(req, res) {
    try {
      const result = await discuss.find({
        product: req.params.id_product
      })
      return res.status(200).json({
        status: 'Success!',
        data: result
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error',
        message: 'Request failed'
      })
    }
  }
  async comment(req, res) {
    try {
      const commentator = await user.findOne({
        where: {
          username: req.user.username
        },
        attributes: ['id', 'username', 'fullName', 'image']
      })
      const answer = await commentator.dataValues
      answer.comment=req.body.comment
      const data = await discuss.findOne({
        _id: req.params.id
      })
      const newComment = data.answer
      newComment.forEach(comment => {
        console.log(comment);
        comment.image=comment.image.substr(5)
      });

      newComment.push(answer)
      discuss.findOneAndUpdate({
        _id:req.params.id
      }, {
        answer:newComment
      }, {
        new: true
      }).then(result => {
        return res.status(200).json({
          status: 'Success!',
          data: result
        })
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error',
        message: 'Request failed'
      })
    }
  }
  async delete(req,res) {
    try {
      const data = await discuss.remove({
        _id: req.params.id
      })
      return res.status(200).json({
        status: 'Success delete data!'
      })
    } catch (e) {
      return res.status(401).json({
        status: 'Error',
        message: 'Request failed'
      })
    }
  }
}

module.exports = new DiscussController
