const {
    product,
    transaction
} = require("../models/mongodb")

const {
    user
} = require("../models/mysql")

class TransactionController {

    async getAll(req, res) {
        //const data = await transaction.deleteMany({}) //untuh hapus isi tabel transaksi
        transaction.find({})
            .then(result => {
                res.json({
                    status: "Success",
                    data: result,
                })
            })
    }

    async getByBuyerUsername(req, res) {
        transaction.find({
                'user.username': req.user.username
            })

            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }

    async getBySellerUsername(req, res) {
        transaction.find({
                'productId.seller.username': req.user.username
            })

            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }

    async notifSeller(req, res) {
        transaction.find({
                'productId.seller.username': req.user.username,
                status: "on process"

            })

            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }

    async getByBuyerorSeller(req, res) {
        transaction.find({
                $or: [{
                    'productId.seller.username': req.user.username
                }, {
                    'user.username': req.user.username
                }]
            })

            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }

    async updateResi(req, res) {
        await transaction.findOneAndUpdate({
            _id: req.params._id
        }, {
            resi: req.body.resi,
            status: "success"
        })
        await transaction.findOne({
            _id: req.params._id
        }).then(async (result) => {
            const cariProduct = await transaction.findOne({
                _id: req.params._id
            }, 'productId')
            const cariTiapProduct = [];
            for (let i = 0; i < cariProduct.productId.length; i++) {
                cariTiapProduct[i] = await product.findOne({
                    _id: cariProduct.productId[i]._id
                })
                await product.findOneAndUpdate({
                    _id: cariProduct.productId[i]._id
                }, {
                    $set: {
                        stock: eval(cariTiapProduct[i].stock) - eval(cariProduct.productId[i].quantity)
                    }
                })

            }
            res.json({
                status: "success update data",
                data: result
            })
        })

    }

    async updatePayment(req, res) {
        await transaction.findOneAndUpdate({
            _id: req.params._id
        }, {
            payment: req.file === undefined ? "" : req.file.filename,
            status: "on process"
        })

        await transaction.findOne({
            _id: req.params._id
        }).then(result => {
            res.json({
                status: "success update data",
                data: result
            })
        })
    }
    
    async create(req, res) {
        let theProducts = [];
        for (let i = 0; i < req.body.productId.length; i++) {
            theProducts[i] = await product.findOne({
                _id: req.body.productId[i]
            }, 'image quantity name seller price');
            theProducts[i].quantity = await eval(req.body.quantity[i]);

        }

        let total = 0;
        theProducts.forEach((value, i) => {
            value.quantity = req.body.quantity[i];
            total += eval(value.price.toString()) * eval(req.body.quantity[i]);
        })
        const theuser = await user.findOne({
            where: {
                username: req.user.username
            },
            attributes: ['id', 'username']
        })
        await transaction.create({
                productId: theProducts,
                user: theuser.dataValues,
                total: total
            })
            .then(async (result) => {
                await cart.delete({
                    username: req.user.username
                })
                res.json({
                    status: "Transaction on pending. Please complete the payment",
                    data: result
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    async delete(req, res) {
        transaction.delete({
            _id: req.params._id
        }).then(result => {
            res.json({
                status: "Delete success",
                data: null
            })
        })
    }

}
module.exports = new TransactionController;
