const {
    cart,
    product,

} = require("../models/mongodb")
const {
    user
} = require("../models/mysql")

class CartController {
    /* async getAll(req, res) {
        //const data = await cart.deleteMany({})
        cart.find({}).populate('productId', 'name price image')
            .then(result => {
                res.json({
                    status: "Success",
                    data: result,
                })
            })
    } */
    async getByusername(req, res) {
        cart.find({
                username: req.user.username
            }).populate('productId', 'name price image')
            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }
    async create(req, res) {
        const data = await product.findOne({
            _id: req.body.productId
        })
        const theuser = await user.findOne({
            where: {
                username: req.user.username
            }
        })


        cart.create({
                productId: data,
                username: theuser.username
            })
            .then(result => {
                res.json({
                    status: "Success add cart",
                    data: result
                })
            })
    }
    /* async update(req, res) {
        cart.findOneAndUpdate({
            _id: req.params._id
        }, {
            quantity: req.body.quantity
            //  totalprice: totalpricebaru

        }).then(() => {
            return cart.findOne({
                _id: req.params._id,
            })
        }).then(result => {
            res.json({
                status: "Success updating data",
                data: result
            })
        })
    } */
    async delete(req, res) {
        cart.delete({
            _id: req.params._id
        }).then(result => {
            res.json({
                status: "Succes delete data",
                data: null
            })
        })
    }
}
module.exports = new CartController;