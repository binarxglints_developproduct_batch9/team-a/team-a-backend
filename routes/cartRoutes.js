const express = require('express') //import express
const router = express.Router() // make router from app
const passport = require('passport');

const CartController = require('../controllers/cartController.js');
const cartValidator = require('../middlewares/validators/cartValidator.js');
const cart = require('../models/mongodb/cart.js');

//router.get('/', CartController.getAll);
router.post('/create', [passport.authenticate('profile', {session:false}), cartValidator.create], CartController.create);
router.get('/',[passport.authenticate('profile', {session:false})], CartController.getByusername); //if accessing localhost:3000/movie , it will call getGenre function in class MovieController
router.delete('/delete/:_id',[passport.authenticate('profile', {session:false}), cartValidator.delete], CartController.delete); //if accessing localhost:3000/movie , it will call getGenre function in class MovieController 

module.exports = router; //Export router

