const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../middlewares/auth');
const userValidator = require('../middlewares/validators/userValidator');
const UserController = require('../controllers/userController');

//Register
router.post('/signup', [userValidator.signup, function(req, res, next) {
  passport.authenticate('signup', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.signup(user, req, res, next);
  })(req, res, next);
}]);

//Google Oauth
router.get('/auth/google', passport.authenticate('google', {session:false, scope: ['profile', 'email'] }));

//Google error
router.get('/failed', UserController.failed)

//Google Oauth Callback
router.get('/auth/google/callback', passport.authenticate('google', {session:false, failureRedirect: '/failed' }), UserController.loginGoogle);

//Verification Email
router.get('/verification/:token', UserController.verification)

//Login
router.post('/login', [userValidator.login, function(req, res, next) {
  passport.authenticate('login', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.login(user, req, res, next);
  })(req, res, next);
}]);

//Switch on/off 2FA
router.put('/switch', [passport.authenticate('profile', {
  session: false
})], UserController.switch)

//2FA
router.get('/2fa', userValidator.twoFA, UserController.twoFA)

//Show Our Profile
router.get('/profile', [passport.authenticate('profile', {
  session: false
})], UserController.getOne)

//Update Profile
router.put('/profile/update', [userValidator.update, passport.authenticate('profile', {
  session: false
})], UserController.update)

//Change Password
router.put('/change_password', [passport.authenticate('profile', {
  session: false
}), userValidator.change_password], UserController.change_password)

//Request link to reset password
router.post('/forgot_password', userValidator.forgot_password, UserController.forgot_password)

//Reset Password
router.put('/reset_password/:token', userValidator.reset_password, UserController.reset_password)

//Show other user profile
router.get('/profile/:username', userValidator.getOne, UserController.getOneO)

//Show All User Data(For Admin)
router.get('/', [passport.authenticate('admin', {
  session: false
})], UserController.getAll)

//Update User via admin
router.put('/update/:username', [userValidator.admin_update, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.admin_update(user, req, res, next);
  })(req, res, next);
}])

//Delete User via admin
router.delete('/delete/:username', [userValidator.delete, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.delete(user, req, res, next);
  })(req, res, next);
}])

//Restore User via admin
router.put('/restore/:username', [userValidator.restore, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.restore(user, req, res, next);
  })(req, res, next);
}])

//Hard delete via admin
router.delete('/hard_delete/:username', [userValidator.delete, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.hard_deleted(user, req, res, next);
  })(req, res, next);
}])

module.exports = router
