const express = require('express')
const router = express.Router()
const passport = require('passport');
const productController = require('../controllers/productController');
const ProductController = require('../controllers/productController')
const productValidator = require('../middlewares/validators/productValidator')

router.get('/', ProductController.getAll)
router.get('/page/carousel', ProductController.carousel)
router.get('/:id', productValidator.getOne, ProductController.getOne)
router.get('/seller/:username', productValidator.getBySeller, productController.getBySeller)
router.get('/category/:category', productValidator.getByCategory, ProductController.getByCategory)
router.get('/find/:keyword', productValidator.getByName, ProductController.getByName)
router.post('/create/', [productValidator.create, passport.authenticate('profile', {
  session: false
})], ProductController.create)
router.put('/update/:id', [productValidator.update, passport.authenticate('profile', {
  session: false
})], ProductController.update)
router.delete('/delete/:id', [productValidator.delete, passport.authenticate('profile', {
  session: false
})], ProductController.delete)

module.exports = router
