const express = require('express')
const router = express.Router()
const passport = require('passport');
const ReviewController = require('../controllers/reviewController')
const reviewValidator = require('../middlewares/validators/reviewValidator')

router.get('/:id', reviewValidator.getByProduct, ReviewController.getByProduct)
router.post('/create/:id', [reviewValidator.create, passport.authenticate('profile', {
  session:false
})], ReviewController.create)
router.put('/update/:id_product/:id_review', [reviewValidator.update, passport.authenticate('profile', {
  session:false
})], ReviewController.update)
router.delete('/delete/:id', [reviewValidator.delete, passport.authenticate('admin', {
  session:false
})], ReviewController.delete)

module.exports = router
