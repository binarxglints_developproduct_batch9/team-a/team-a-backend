const express = require('express') //import express
const router = express.Router() // make router from app
const passport = require('passport');
const TransactionController = require('../controllers/transactionController.js');
const transactionValidator = require('../middlewares/validators/transactionValidator.js');

router.get('/', TransactionController.getAll);
router.get('/buyer/', [passport.authenticate('profile', {session:false})], TransactionController.getByBuyerUsername);
router.get('/seller/',[passport.authenticate('profile', {session:false})], TransactionController.getBySellerUsername);
router.get('/seller/notif',[passport.authenticate('profile', {session:false})], TransactionController.notifSeller); 
router.get('/buyerseller/', [passport.authenticate('profile', {session:false})], TransactionController.getByBuyerorSeller);
router.post('/create', [passport.authenticate('profile', {session:false})],  transactionValidator.create, TransactionController.create);
router.put('/update/resi/:_id',[passport.authenticate('profile', {session:false}), transactionValidator.updateResi], TransactionController.updateResi);
router.put('/update/payment/:_id',[passport.authenticate('profile', {session:false}), transactionValidator.updatePayment], TransactionController.updatePayment);
router.delete('/delete/:_id',[passport.authenticate('profile', {session:false}), transactionValidator.delete], TransactionController.delete)

module.exports = router; //Export router

