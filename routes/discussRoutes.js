const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../middlewares/auth');
const discussValidator = require('../middlewares/validators/discussValidator');
const DiscussController = require('../controllers/discussController');

//Start discuss (create question)
router.post('/:id_product/create', [discussValidator.create, passport.authenticate('profile', {
  session: false
})], DiscussController.create );

//Show discussion by id product
router.get('/:id_product', [discussValidator.show, passport.authenticate('profile', {
  session: false
})], DiscussController.show)

//Add comment (edit discussion)
router.put('/:id/comment', [discussValidator.comment, passport.authenticate('profile', {
  session: false
})], DiscussController.comment)

//Delete discussion via admin
router.delete('/delete/:id', [discussValidator.delete, passport.authenticate('admin', {
  session: false
})], DiscussController.delete)


module.exports = router
