const express = require('express');
const cron = require('node-cron'); //import cron module to schedule a task
const cronJob = require('./cron') //import file cron.js
const cors = require('cors')
const bodyParser = require('body-parser');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes')
const cartRoutes = require('./routes/cartRoutes.js')
const discussRoutes = require('./routes/discussRoutes')
const reviewRoutes = require('./routes/reviewRoutes')
const transactionRoutes = require('./routes/transactionRoutes.js')


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cors())

app.use(express.static('public'));

app.use('/', userRoutes);
app.use('/home', productRoutes)
app.use('/cart', cartRoutes)
app.use('/discuss', discussRoutes)
app.use('/review', reviewRoutes)
app.use('/transaction', transactionRoutes);

cron.schedule('* * * * * *', async () => {
  cronJob.rProviderPay()
})

app.listen(3003, ()=>{
  console.log('User running on port 3003');
})

module.exports = app
