const {
  user
} = require('./models/mysql')

const {
  transaction,
} = require('./models/mongodb')

//const nodemailer = require('nodemailer');
const cron = require('node-cron')

//file ini blum DIBUAT
//const MailController = require('./controllers/mailController')

async function rProviderPay() {
  const today = new Date()
  const unpaidTransaction = await transaction.findOne({
    status: "pending"
  })

  /*     if (unpaidTransaction.length == 0) {
          return
      } */
  //console.log(unpaidTransaction.created_at);
  //console.log(today);
  if (unpaidTransaction) {
    if ((today - unpaidTransaction.created_at) / 1000 / 60 / 60 > 2) {
      // MailController.rProviderPay

      await transaction.findOneAndUpdate({
        _id: unpaidTransaction._id
      }, {
        status: "canceled"
      })
    }
  }
}

module.exports.rProviderPay = rProviderPay
//rProviderPay();
